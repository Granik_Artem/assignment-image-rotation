#ifndef BMP_H
#define BMP_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "image.h"

// Структура - заголовок BMP файла

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};


struct bmp_header image_header(const struct image* img);

// Перечисление возможных результатов чтения из файла

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};


enum read_status from_bmp( FILE* in, struct image* img );

// Массив сообщений о результатах чтения из файла

static const char* read_status_array[] = {
        [READ_OK] = "Successful reading!\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature.\n",
        [READ_INVALID_BITS] = "Invalid bits.\n",
        [READ_INVALID_HEADER] = "Invalid bmp-file header.\n",
        [READ_ERROR] = "Failed to read from file.\n"
};

void read_status_message(enum read_status rs);

//Перечисление возможных результатов записи в файл

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

// Массив сообщений о результатах записи в файл

static const char* write_status_array[] = {
    [WRITE_OK] = "Successful writing!\n",
    [WRITE_ERROR] = "Error occurred during writing.\n"
};

void write_status_message(enum write_status ws);

#endif //BMP_H
