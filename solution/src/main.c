#include <stdio.h>

#include "rotation.h"
#include "bmp.h"
#include "filehandler.h"
int main( int argc, char** argv ){
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if(argc != 3){
        fprintf(stderr, "Wrong amount of arguments");
        abort();
    }
    // Инициализация и открытие файла-источника

    FILE *input;
    open_status_message(f_open(argv[1], &input, 0));

    // Инициализация изображения и чтение из файла

    struct image source;
    read_status_message(from_bmp(input, &source));

    // Закрытие файла-источника

    close_status_message(f_close(input));

    // Поворот изображения на 90 градусов против часовой стрелки

    struct image result = rotate_90degrees_ccw(source);

    // Освобождение памяти, выделенной под изначальное сообщение

    delete_image(&source);

    // Инициализация и открытие/создание выходного файла для записи в него изображения

    FILE *output = {0};
    open_status_message(f_open(argv[2], &output, 1));

    // Запись изображения в выходной файл

    write_status_message(to_bmp(output,&result));

    // Освобождение памяти, выделенной под повернутое изображения

    delete_image(&result);

    // Закрытие выходного файла

    close_status_message(f_close(output));
    return 0;
}
