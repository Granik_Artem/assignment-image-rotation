#include "image.h"


// Функция создания картинки с заданной шириной и высотой

struct image create_image(uint64_t width, uint64_t height){
    struct image res =  {
            .width = width,
            .height =height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
    return res;
}

// Функция для удаления памяти, зарезервированной под картинку

void delete_image(struct image* image){
    free(image -> data);
}
