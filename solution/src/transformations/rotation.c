#include "rotation.h"


// Функция, отвечающая за поворот изображения на 90 градусов против часовой стрелки (ccw - counterclockwise)

struct image rotate_90degrees_ccw( struct image const source ){
    struct image res = create_image(source.height, source.width);
    if(sizeof(res) == sizeof(source)) {
        uint64_t counter = 0;
        for (int64_t i = res.width - 1; i >= 0; i--) {
            for (int64_t j = 0; j < res.height; j++) {
                res.data[j * res.width + i] = source.data[counter];
                counter++;
            }
        }
        return res;
    }else{
        fprintf(stderr, "Failed to allocate memory");
        abort();
    }
}



