#include "filehandler.h"

// Функция открытия файла, возвращающая в качестве результата статус открытия файла.
// Аргумент mode определяет, открывается ли файл на запись или для чтения.

enum open_status f_open(char const *file_name, FILE** in, int mode){
    if(mode == 0) {
        *in = fopen(file_name, "r+");
    }else {
        if (mode == 1) {
            *in = fopen(file_name, "w+");
        }
    }
    if(in != NULL){
        return OPEN_OK;
    }else{
        return OPEN_ERROR;
    }
}

// Функция для вывода сообщения о результатах открытия файла в stderr

void open_status_message(enum open_status os){
    fprintf(stderr,"%s", open_status_array[os]);
    if(os != 0){
        abort();
    }
}

// Функция открытия файла, возвращающая в качестве результата статус закрытия файла

enum close_status f_close( FILE* out){
    if(fclose(out) == 0){
        return CLOSE_OK;
    }else{
        return CLOSE_ERROR;
    }
}

// Функция для вывода сообщения о результатах закрытия файла в stderr

void close_status_message(enum close_status cs){
    fprintf(stderr,"%s", close_status_array[cs]);
    if(cs != 0){
        abort();
    }
}
